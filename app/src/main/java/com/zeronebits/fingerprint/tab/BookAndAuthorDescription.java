package com.zeronebits.fingerprint.tab;

/**
 * Created by neero on 29/03/2017.
 */
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zeronebits.fingerprint.R;
import com.zeronebits.fingerprint.database.DatabaseController;
import com.zeronebits.fingerprint.entity.Book;

import java.util.ArrayList;

/**
 * Created by hp1 on 21-01-2015.
 */
public class BookAndAuthorDescription extends Fragment {


    DatabaseController databaseController;
    ArrayList<Book> bookArrayList;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v =inflater.inflate(R.layout.book_author_description,container,false);
        Bundle bundle = this.getArguments();
        int position = bundle.getInt("position");
        int bookPosition = bundle.getInt("bookPosition");
        TextView description = (TextView)v.findViewById(R.id.description);
        databaseController = new DatabaseController(getActivity());
        bookArrayList = databaseController.getBookList();

        Book book = bookArrayList.get(bookPosition);

        if(position == 0){
            description.setText(book.getBookDescription());
        }else{
            description.setText(book.getPublisherName());
        }

        return v;
    }
}
