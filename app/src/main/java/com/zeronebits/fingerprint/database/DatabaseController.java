package com.zeronebits.fingerprint.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


import com.zeronebits.fingerprint.entity.Book;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by neero on 01/08/2016.
 */
public class DatabaseController extends SQLiteOpenHelper {


    public DatabaseController(Context applicationcontext) {
        super(applicationcontext, "fingerprint.db", null, 02);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query1;

        query1 = "CREATE TABLE book ( book_id INTEGER, book_name VARCHAR, book_isbn VARCHAR,book_description VARCHAR, book_price VARCHAR, " +
                "book_photo VARCHAR, book_pages VARCHAR, book_publisher VARCHAR, book_publish_date VARCHAR, book_edition VARCHAR)";
        db.execSQL(query1);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        String query1;
        query1 = "DROP TABLE IF EXISTS book";
        db.execSQL(query1);
        onCreate(db);
    }


    public void insertBookList(HashMap<String, String> queryValues) {
        SQLiteDatabase database = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("book_id", queryValues.get("book_id"));
        values.put("book_name", queryValues.get("book_name"));
        values.put("book_isbn", queryValues.get("book_isbn"));
        values.put("book_description", queryValues.get("book_description"));
        values.put("book_price", queryValues.get("book_price"));
        values.put("book_photo", queryValues.get("book_photo"));
        values.put("book_pages", queryValues.get("book_pages"));
        values.put("book_publisher", queryValues.get("book_publisher"));
        values.put("book_publish_date", queryValues.get("book_publish_date"));
        values.put("book_edition", queryValues.get("book_edition"));

        database.insert("book", null, values);
        Log.d("Checking","added");
        database.close();

    }

    public void deleteAllFromTable() {
        SQLiteDatabase database = this.getWritableDatabase();
        String selectQuery = "DELETE FROM book";
        database.execSQL(selectQuery);
    }

    public ArrayList<Book> getBookList(){
        ArrayList<Book> allData = new ArrayList<>();

        SQLiteDatabase database = this.getWritableDatabase();
        String selectQuery = "SELECT  * FROM book";
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                Book book = new Book();
                book.setBookId(Integer.parseInt(cursor.getString(0)));
                book.setBookName(cursor.getString(1));
                book.setBookISBN(cursor.getString(2));
                book.setBookDescription(cursor.getString(3));
                book.setBookPrice(cursor.getString(4));
                book.setBookPhoto(cursor.getString(5));
                book.setBookPages(cursor.getString(6));
                book.setPublisherName(cursor.getString(7));
                book.setPublishDate(cursor.getString(8));
                book.setBookEdition(cursor.getString(9));

                allData.add(book);
            }
        }
        database.close();
        return allData;
    }

/*
    public EntityForRentDetail getHomeDetail(int id){
        EntityForRentDetail entityForRentDetail = new EntityForRentDetail();
        SQLiteDatabase database = this.getWritableDatabase();
        String selectQuery = "SELECT  * FROM kothacha WHERE house_id = '"+id+"' ";
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                Log.d("StringLog",cursor.getString(0));
                entityForRentDetail.setHouseId(Integer.parseInt(cursor.getString(0)));
                entityForRentDetail.setNoOfRoom(Integer.parseInt(cursor.getString(1)));
                entityForRentDetail.setDescription(cursor.getString(2));
                entityForRentDetail.setStreetAddress(cursor.getString(3));
                entityForRentDetail.setAddress(cursor.getString(4));
                entityForRentDetail.setLongitude(Double.parseDouble(cursor.getString(5)));
                entityForRentDetail.setLatitude(Double.parseDouble(cursor.getString(6)));
                entityForRentDetail.setStatus(cursor.getString(7));
                entityForRentDetail.setPhone(cursor.getString(8));
                entityForRentDetail.setUserName(cursor.getString(9));
                entityForRentDetail.setPrice(cursor.getString(10));
                entityForRentDetail.setNegotiateStatus(cursor.getString(11));
                entityForRentDetail.setOtherDetail(cursor.getString(12));
            }
        }
        database.close();
        return entityForRentDetail;
    }*/
}
