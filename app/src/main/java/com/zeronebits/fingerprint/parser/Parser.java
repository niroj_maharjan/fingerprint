package com.zeronebits.fingerprint.parser;

import android.content.Context;
import android.util.Log;


import com.zeronebits.fingerprint.database.DatabaseController;
import com.zeronebits.fingerprint.entity.Book;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by neero on 08/08/2016.
 */
public class Parser {
    public HashMap<String, String> map;
    Context context;
    public static ArrayList<String> images;

    public Parser(Context context) {
        this.context = context;
    }

    public ArrayList<Book> AllParse(JSONArray result) {
        Log.d("CheckingResult0", result.toString());

        DatabaseController databaseController = new DatabaseController(context);
        ArrayList<Book> allData = new ArrayList<>();
        try {
            Log.d("CheckingResult2", result.length() + "");
            databaseController.deleteAllFromTable();
            for (int i = 0; i < result.length(); i++) {
                map = new HashMap<String, String>();
                JSONObject response = result.getJSONObject(i);
                map.put("book_id", response.getString("book_id"));
                map.put("book_name", response.getString("book_name"));
                map.put("book_isbn", response.getString("book_isbn"));
                map.put("book_description", response.getString("book_description"));
                map.put("book_price", response.getString("book_price"));
                map.put("book_photo", response.getString("book_photo"));
                map.put("book_pages", response.getString("book_pages"));
                map.put("book_publisher", response.getString("publisher_name"));
                map.put("book_publish_date", response.getString("book_publish_date"));
                map.put("book_edition", response.getString("book_edition"));

                databaseController.insertBookList(map);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            return allData;
        }
    }

}

