package com.zeronebits.fingerprint.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.github.florent37.materialviewpager.header.MaterialViewPagerHeaderDecorator;
import com.zeronebits.fingerprint.R;
import com.zeronebits.fingerprint.activities.BookDetails;
import com.zeronebits.fingerprint.application.AppController;
import com.zeronebits.fingerprint.database.DatabaseController;
import com.zeronebits.fingerprint.entity.Book;
import com.zeronebits.fingerprint.adapter.TestRecyclerViewAdapter;
import com.zeronebits.fingerprint.parser.Parser;
import com.zeronebits.fingerprint.utils.ConnectionDetector;
import com.zeronebits.fingerprint.utils.LinkConfig;
import com.zeronebits.fingerprint.utils.RecyclerItemClickListener;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by florentchampigny on 24/04/15.
 */
public class RecyclerViewFragment extends Fragment {

    static final boolean GRID_LAYOUT = false;
    private static final int ITEM_COUNT = 100;
    private static final String TAG ="RecyclerViewFragment";
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private List<Object> mContentItems = new ArrayList<>();
    ConnectionDetector connectionDetector;
    DatabaseController databaseController;
    ArrayList<Book> bookList;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_recyclerview, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager layoutManager;
        Bundle bundle = this.getArguments();

        int position = bundle.getInt("position");
//        Toast.makeText(getActivity(), ""+position, Toast.LENGTH_SHORT).show();

        connectionDetector = new ConnectionDetector(getActivity());
        databaseController = new DatabaseController(getActivity());
        try {
            bookList = databaseController.getBookList();
            displayDetails(bookList);
            if(connectionDetector.isConnectingToInternet()) {
                makeJsonObjectRequest();
            }else{
                Snackbar snack = Snackbar.make(mRecyclerView, "No Internet", Snackbar.LENGTH_LONG);
                View snackBarView = snack.getView();
                snackBarView.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.red));
                snack.show();
            }
        }catch (Exception e) {
            if (connectionDetector.isConnectingToInternet()) {
//                loadingLayout.setVisibility(View.VISIBLE);
//                startAnim();
                makeJsonObjectRequest();
            } else {
                Snackbar snack = Snackbar.make(mRecyclerView, "No Internet", Snackbar.LENGTH_LONG);
                View snackBarView = snack.getView();
                snackBarView.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.red));
                snack.show();
            }
        }
        if (GRID_LAYOUT) {
            layoutManager = new GridLayoutManager(getActivity(), 2);
        } else {
            layoutManager = new LinearLayoutManager(getActivity());
        }
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);

        //Use this now
        mRecyclerView.addItemDecoration(new MaterialViewPagerHeaderDecorator());


        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {

                        Intent intent = new Intent(getActivity(), BookDetails.class);
                        intent.putExtra("position",position);
                        /*ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                                // the context of the activity
                                getActivity(),
                                new Pair<View, String>(view.findViewById(R.id.bookName), getString(R.string.transition_name_title))

                        );
                        ActivityCompat.startActivity(getActivity(), intent, options.toBundle());*/
                        startActivity(intent);

                    }
                })
        );


        if(position != 0){
            mRecyclerView.setVisibility(View.GONE);
        }else{
            mRecyclerView.setVisibility(View.VISIBLE);
        }

//
    }


    private void makeJsonObjectRequest() {

//        showpDialog();

        JsonArrayRequest jsonObjReq = new JsonArrayRequest(Request.Method.GET,
                LinkConfig.DETAIL_URL, null, new Response.Listener<JSONArray>() {

            @Override
            public void onResponse(JSONArray response) {
                Log.d(TAG, response.toString());
                Parser parser = new Parser(getActivity());
                parser.AllParse(response);
                bookList = databaseController.getBookList();
                displayDetails(bookList);

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Snackbar snack = Snackbar.make(mRecyclerView, "Cannot Refresh", Snackbar.LENGTH_LONG);
                View snackBarView = snack.getView();
                snackBarView.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.red));
                snack.show();
                // hide the progress dialog
//                hidepDialog();
            }
        });


        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }

    private void displayDetails(ArrayList<Book> books) {

        mAdapter = new TestRecyclerViewAdapter(books, getActivity());
        mAdapter.notifyDataSetChanged();

        mRecyclerView.setAdapter(mAdapter);


    }

}