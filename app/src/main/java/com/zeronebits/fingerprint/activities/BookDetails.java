package com.zeronebits.fingerprint.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Shader;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.zeronebits.fingerprint.R;
import com.zeronebits.fingerprint.adapter.CustomPagerAdapter;
import com.zeronebits.fingerprint.adapter.ViewPagerAdapterBookDetail;
import com.zeronebits.fingerprint.database.DatabaseController;
import com.zeronebits.fingerprint.entity.Book;
import com.zeronebits.fingerprint.tab.SlidingTabLayout;

import java.util.ArrayList;


/**
 * Created by neero on 26/03/2017.
 */
public class BookDetails extends AppCompatActivity {

    CustomPagerAdapter mCustomPagerAdapter;
    ViewPager mViewPager,slidingTabViewPager;
    DatabaseController databaseController;
    ArrayList<Book> bookArrayList;
    TextView releaseDate,language,tags,pages,bookName,authorName;
    ViewPagerAdapterBookDetail viewPagerBookDetail;
    CharSequence Titles[]={"About The Book","About The Author"};
    int Numboftabs =2;
    SlidingTabLayout tabs;
    Book book;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.book_detail);
        databaseController = new DatabaseController(this);
        bookArrayList = databaseController.getBookList();
        mCustomPagerAdapter = new CustomPagerAdapter(BookDetails.this,bookArrayList);
        Bundle bundle = getIntent().getExtras();
        int position = bundle.getInt("position");
        findViews();
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setClipToPadding(false);
        // set padding manually, the more you set the padding the more you see of prev & next page
        mViewPager.setPageMargin(-400);
        mViewPager.setAdapter(mCustomPagerAdapter);
        mViewPager.setCurrentItem(position);
        slidingTabViewPager = (ViewPager) findViewById(R.id.bookdetailViewPager);
        book = bookArrayList.get(position);
        setTabValue(position);
        setValue(book);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
          @Override
          public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

          }

          @Override
          public void onPageSelected(int position) {
              book = bookArrayList.get(position);
              setValue(book);
              setTabValue(position);
          }

          @Override
          public void onPageScrollStateChanged(int state) {

          }
      });



    }





    public void setTabValue(int position){
        viewPagerBookDetail =  new ViewPagerAdapterBookDetail(getSupportFragmentManager(),Titles,Numboftabs,position);

        // Assigning ViewPager View and setting the adapter
        slidingTabViewPager.setAdapter(viewPagerBookDetail);

        // Assiging the Sliding Tab Layout View
        tabs = (SlidingTabLayout) findViewById(R.id.tabs);
        tabs.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width
        tabs.setViewPager(slidingTabViewPager);
    }
    
    private void findViews() {
        releaseDate = (TextView)findViewById(R.id.release_date);
        language = (TextView)findViewById(R.id.language);
        tags = (TextView)findViewById(R.id.tags);
        pages = (TextView)findViewById(R.id.pages);
        bookName = (TextView)findViewById(R.id.bookName);
        authorName = (TextView)findViewById(R.id.authorName);
    }


    public void setValue(Book book){
        releaseDate.setText(book.getPublishDate());
        language.setText("Nepali");
        tags.setText(book.getBookEdition());
        pages.setText(book.getBookPages());
        bookName.setText(book.getBookName());
        authorName.setText(book.getPublisherName());
    }


}
