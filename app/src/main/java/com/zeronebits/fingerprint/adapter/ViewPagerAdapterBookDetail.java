package com.zeronebits.fingerprint.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.zeronebits.fingerprint.tab.BookAndAuthorDescription;

/**
 * Created by neero on 29/03/2017.
 */
public class ViewPagerAdapterBookDetail extends FragmentStatePagerAdapter {

    CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
    int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created
    int bookPosition;

    // Build a Constructor and assign the passed Values to appropriate values in the class
    public ViewPagerAdapterBookDetail(FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb,int bookPosition) {
        super(fm);

        this.Titles = mTitles;
        this.NumbOfTabs = mNumbOfTabsumb;
        this.bookPosition = bookPosition;
    }

    //This method return the fragment for the every position in the View Pager
    @Override
    public Fragment getItem(int position) {
        switch (position % 4) {
            //case 0:
            //    return RecyclerViewFragment.newInstance();
            //case 1:
            //    return RecyclerViewFragment.newInstance();
            //case 2:
            //    return WebViewFragment.newInstance();
            default:
                Fragment fragment = null;
                fragment = new BookAndAuthorDescription();
                Bundle args = new Bundle();
                args.putInt("position",position);
                args.putInt("bookPosition",bookPosition);
                fragment.setArguments(args);
                return fragment;
        }
    }

    // This method return the titles for the Tabs in the Tab Strip

    @Override
    public CharSequence getPageTitle(int position) {
        return Titles[position];
    }

    // This method return the Number of tabs for the tabs Strip

    @Override
    public int getCount() {
        return NumbOfTabs;
    }
}
