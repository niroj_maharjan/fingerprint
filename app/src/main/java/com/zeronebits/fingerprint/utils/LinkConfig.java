package com.zeronebits.fingerprint.utils;

/**
 * Created by neero on 08/08/2016.
 */
public class LinkConfig {

    public static String BASE_URL = "http://www.zeronebits.com/zerone_bits/public/api/";
    public static String DETAIL_URL = BASE_URL + "publishers/books?publisher_id=1";
    public static String IMAGE_DETAIL = BASE_URL + "image_json.php";
    public static String UPLOAD_IMAGE = BASE_URL + "upload.php";
}
